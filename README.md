# Tutorial Django + Postgresql (CI/CD)


## Introduction

Overview of steps to configure PostgreSQL database with Django, then you have landed in the right place. This guide will teach you how to integrate PostgreSQL with Django for your applications. In addition to that, we briefly skim through PostgreSQL and Django.

This repo contains code to deploy a Django app with Postgresql database using CI/CD and Dockirization using Dockerfile and Docker-Compose.

## Project setup

```
|---- quickstart
      |---- migrations
      |---- __init__.py
      |---- admin.py
      |---- apps.py
      |---- models.py
      |---- tests.py
      |---- views.py
|---- tutorial
      |---- __pycache__
      |---- __init__.py
      |---- asgi.py
      |---- settings.py
      |---- urls.py
      |---- wsgi.py
|---- .gitlab-ci.yml
|---- docker-compose.yml
|---- Dockerfile
|---- manager.py
|---- requirements.txt

```
Let us have a brief explanation of the structure of the project.

`tutorial` files inside this folder are for project configuration, for example `urls.py` will contain your APIs URL. 

`asgi.py` is for support deploying on ASGI, the emerging Python standard for asynchronous web servers and applications.
`wsgi.py` is Django’s primary deployment platform is WSGI, the Python standard for web servers and applications.

`quickstart` under this folder we can create our APIs with model classes.

`models.py` a model is the single, definitive source of information about your data. It contains the essential fields and behaviors of the data you’re storing. Generally, each model maps to a single database table.

`view.py` ,a view function, or view for short, is a Python function that takes a web request and returns a web response. This response can be the HTML contents of a web page, or a redirect, a 404 error, an XML document, or an image . . . or anything.

`test.py` is Django’s unit tests that use a Python standard library module: unit test. This module defines tests using a class-based approach.

`admin.py` ,one of the most powerful parts of Django, is the automatic admin interface. It reads metadata from your models to provide a quick, model-centric interface where trusted users can manage content on your site.

`app.py` Django contains a registry of installed applications that stores configuration and provides introspection. It also maintains a list of available models.


`manage.py` is automatically created in each Django project. It does the same thing as Django-admin but also sets the `DJANGO_SETTINGS_MODULE` environment variable to point to your project’s settings.py file.

`requirements.txt` contains all the necessary independence to install to allow the connection between  Django and PostgreSQL.

`psycopg2` is the most popular PostgreSQL database adapter for the Python programming language. Its main features are the complete implementation of the Python DB API 2.0 specification and the thread safety (several threads can share the same connection). It was designed for heavily multi-threaded applications that create and destroy many cursors and make many concurrent “INSERT”s or “UPDATE”s. 

Psycopg 2 is mainly implemented in C as a libpq wrapper, resulting in being both efficient and secure. It features client-side and server-side cursors, asynchronous communication and notifications, and “COPY TO/COPY FROM” support. Many Python types are supported out-of-the-box and adapted to match PostgreSQL data types; adaptation can be extended and customized thanks to a flexible object adaptation system.


`settings.py` contains the configuration of a Django project and the configuration to connect the Django application to the database.

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'root',
        'HOST': '0.0.0.0',
        'PORT': '5432',
    }
}
```

`Engine?`

As you can see in the settings.py file, we insert PostgreSQL instead of SQLite.

`Name?`

The database does not have a name, but you must assign one to access the database.
If no name is given, the provider accepts 'postgres' as the name of the database.

`Username and Password?`

Insert the username and password that you specified when you created the database.

`Host? Port?`

As you can see in the settings.py file, we inserted PostgreSQL instead of SQLite and inserted the username and password that we specified when we created the database.

Once we have made the changes in settings.py, we must run a migration in our virtual environment before the changes take place:

```python
py manage.py migrate
```




The other files `docker-compose.yml`, `Dockerfile` and `.gitlab-ci.yml`, are for the deployment. We will speak about them later!


## Dockerfile
Docker can build images automatically by reading the instructions from a Dockerfile. A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. This page describes the commands you can use in a Dockerfile.
```yml
FROM python:3.9.1-alpine


RUN apk add --update --no-cache \
    g++ gcc libxslt-dev musl-dev python3-dev \
    libffi-dev openssl-dev jpeg-dev zlib-dev postgresql-dev=13.8-r0

ENV LIBRARY_PATH=/lib:/usr/lib


# add and change to non-root user in image
ENV HOME=/home/worker
RUN adduser --home ${HOME} --disabled-password worker
RUN chown worker:worker ${HOME}
USER worker

# Set the working directory for any following RUN, CMD, ENTRYPOINT, COPY and ADD instructions
WORKDIR ${HOME}


COPY ./requirements.txt ${HOME}

RUN pip install --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt

COPY . ${HOME}


```
The `postgresql-server-dev-all` package provides the pg_buildext script to simplify the packaging of a PostgreSQL extension that supports multiple major product versions. The second script, dh_make_pgxs creates a pg_buildext based template for the debian/ directory.

## docker-compose.yml

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.
```yml
version: "3.2"

services:
  postgresql:
    container_name: postgresql
    image: postgres
    shm_size: "3gb"
    deploy:
      resources:
        limits:
          memory: 500M
        reservations:
          memory: 100M
    volumes:
      - postgresql_db:/var/lib/postgresql/data
    ulimits:
      memlock:
        soft: -1
        hard: -1 
    
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=root

    ports:
      - "5432:5432"

  django:
    container_name: django
    restart: always
    build: ./
   
    ports:
      - "8000:8000"

    depends_on:
      - postgresql

    command: >
      sh -c " sleep 20 &&
      python manage.py makemigrations &&
      python manage.py migrate &&
      python manage.py runserver  0.0.0.0:8000"

    volumes:
      - media:/usr/src/app
      
volumes:
  postgresql_db:
  media:
  static:

```

As I explained in the Postgres Docker Compose comments, the environment variables are defined in the environment part of the configuration.
If you copy/paste the environment variables from the Docker Compose file to your settings.py file, you will have the following:

```yml
environment:
  - POSTGRES_DB=postgres
  - POSTGRES_USER=postgres
  - POSTGRES_PASSWORD=root
```


## Postresql Database migration 

After running the docker-compose, we can see these command lines, which are responsible for migrating the models into the PostgreSQL database

```py
python manage.py makemigrations
```
```py
python manage.py migrate
```

# Migrations
Migrations are Django’s way of propagating changes you make to your models (adding a field, deleting a model, etc.) into your database schema. They’re designed to be mostly automatic, but you’ll need to know when to make migrations when to run them, and the common problems you might run into.

The Commands
There are several commands which you will use to interact with migrations and Django’s handling of database schema:

- `migrate`, which is responsible for applying and unapplying migrations.

- `makemigrations`, which is responsible for creating new migrations based on the changes you have made to your models.

- `sqlmigrate`, which displays the SQL statements for a migration.

- `showmigrations`, which lists a project’s migrations and their status.



You should think of migrations as a version control system for your database schema. Make migrations is responsible for packaging up your model changes into individual migration files - analogous to commits - and migrate is responsible for applying those to your database.


## .gitlab-ci.yml

GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever the developer pushes code to the application. GitLab CD (Continuous Deployment) is a software service that places the changes of every code in the production, resulting in every day of production deployment.

# The Container Registry

When docker images are built-in CI/CD pipelines, they need to be pushed somewhere, where they are stored and from where they can be reaccessed. This location is the so-called registry. Gitlab offers such a location in each project. This is the so-called container registry.

The container registry can be found under <b>Menu > Packages & Registries > Container Registry</b>

By default, the container registry is enabled for project under <b>Menu > Settings > General > Visibility, project features, permissions > Container Registry</b>

In CI/CD jobs the container registry can be accessed via
<ul>
<li><code>$CI_REGISTRY</code>. This is the URL to the GitLab registry server:
<br/>registry.gitlab.com</li>
<li><code>$CI_REGISTRY_IMAGE</code>. This is the URL to the GitLab registry of the project, which is for this project: 
<br/>registry.gitlab.com/topfreelance/kurt/back_end/example-app</li>
</ul>
After each successful CI/CD pipeline run, a new docker image is stored in the container registry. For this reason, the used space grows over time. GitLab offers a feature to easily clean up old images, which are not needed anymore. This feature is disabled by default. This project is enabled and configured to do a daily clean-up and store only the last 5 images. This can be done under <b>Menu > Settings > Packages & Registries > Container Registry</b>. 

A deploy token must be created to secure access to the container registry.



<h3>The Deploy Token</h3>
As a first step, a deploy token was created under: <b>Menu > Settings > Repository > Deploy Token</b>. This token was given read/write access to the container registry and read access to the repo, i.e., the variables <code>read_repository</code>, <code>read_registry</code>, and <code>write_registry</code> are set. 

In the next step, that name, user, and token were stored in the file <code>deploy_token.txt</code> in the folder secrets inside the repository.  This local folder was then immediately added to the <code>.gitignore</code> file, to not push that file to the GitLab server.

There is a special deploy token for usage of CI/CD jobs: Once a deploy token with the special name <code>gitlab-deploy-token</code>is created, it is automatically exposed to the CI/CD jobs in the following two CI/CD variables
<ul>
<li>Its username as <code>CI_DEPLOY_USER</code></li>
<li>Its token as <code>CI_DEPLOY_PASSWORD</code></li>
</ul>
This allows to login in CI/CD jobs to the container registry of the project via 

```docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY```

See also the GitLab documentation for more information about Deploy Tokens: https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html




```yml
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml

# Build a Docker image with CI/CD and push to the GitLab registry.
# Docker-in-Docker documentation: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
#
# This template uses one generic job with conditional builds
# for the default branch and all other (MR) branches.


#------------------------------------------------------------
# Stages
#------------------------------------------------------------
stages:
  - test
  - build

#------------------------------------------------------------
# Global Variables
#------------------------------------------------------------
variables:
  APP_NAME: "django-app"
  TAG_LATEST: "latest"
  IMAGE_TAG_SHA: $CI_REGISTRY_IMAGE/$APP_NAME:$CI_COMMIT_SHORT_SHA
  IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE/$APP_NAME:$TAG_LATEST


# test image build
test:image:build:
  stage: test

  image: tmaier/docker-compose:latest

  services:
    - docker:dind

  before_script:
    - docker info
    - docker-compose --version

  script:
    - docker-compose build

#------------------------------------------------------------
# stage: build
#------------------------------------------------------------
# build job. push to container registry
build-job:      
  stage: build

  image:
    name: docker:latest
  
  services: 
    - docker:dind
  
  before_script:
    # login gitlab registry of project
    - mkdir -p $HOME/.docker/
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_DEPLOY_USER}:${CI_DEPLOY_PASSWORD} | base64 | tr -d '\n')\"}}}" > $HOME/.docker/config.json

  script:
    # build and push a new image  
    - docker build -t $IMAGE_TAG_SHA .
    - docker push $IMAGE_TAG_SHA
    - docker image tag $IMAGE_TAG_SHA $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST



```   

## Usage

Run services in the background:
`docker-compose up -d`

Run services in the foreground:
`docker-compose up --build`

Inspect volume:
`docker volume ls`
and
`docker volume inspect <volume name>`

Prune unused volumes:
`docker volume prune`

View networks:
`docker network ls`

Bring services down:
`docker-compose down`

Open a bash session in a running container:
`docker exec -it <container ID> /bin/bash`

